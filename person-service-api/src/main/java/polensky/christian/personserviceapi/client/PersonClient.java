package polensky.christian.personserviceapi.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import polensky.christian.personserviceapi.aggregates.PersonDTO;
import polensky.christian.personserviceapi.aggregates.PersonSaveResponse;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class PersonClient {

    private final String baseUrl;
    private RestTemplate restTemplate;

    public PersonClient(String baseUrl) {
        this.baseUrl = baseUrl;
        init();
    }

    public ResponseEntity<PersonDTO> getPerson(long id) throws URISyntaxException {
        URI uri = new URI(baseUrl + id);
        return restTemplate.getForEntity(uri, PersonDTO.class);
    }

    public List<PersonDTO> getPersons() throws URISyntaxException {

        URI uri = new URI(baseUrl);
        return restTemplate.exchange(uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<PersonDTO>>(){})
                .getBody();
    }

    public List<PersonDTO> getPersonsByColor(String colorString) throws URISyntaxException {

        URI uri = new URI(baseUrl + "color/" + colorString);
        return restTemplate.exchange(uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<PersonDTO>>(){})
                .getBody();
    }

    public PersonSaveResponse savePersons(List<PersonDTO> personDTOs) throws URISyntaxException {
        URI uri = new URI(baseUrl);
        return restTemplate.postForObject(uri, personDTOs, PersonSaveResponse.class);
    }

    private void init() {
        restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(getMessageConverters());
    }

    private List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new MappingJackson2HttpMessageConverter());
        return converters;
    }
}
