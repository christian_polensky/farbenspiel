package polensky.christian.personserviceapi.aggregates;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class PersonSaveResponse {

    @JsonCreator
    public PersonSaveResponse() {
    }

    public PersonSavingStatus status;
    public List<PersonDTO> savedDTOs;
    public List<String> errors;
}
