package polensky.christian.personserviceapi.aggregates;

public enum PersonSavingStatus {
    SUCCESS, FAILED, PARTIALLY_FAILED
}
