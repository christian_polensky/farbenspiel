package polensky.christian.personserviceapi.aggregates;

import com.fasterxml.jackson.annotation.JsonCreator;

public class PersonDTO {

    public String name;
    public String surname;
    public String city;
    public String zipcode;
    public String color;

    @JsonCreator
    public PersonDTO() {}

}
