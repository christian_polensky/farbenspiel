package polensky.christian.personserviceapi.aggregates;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

class PersonSaveResponseTest {

    @Test
    void mustBeSerializableAndDeserializable() throws IOException {
        PersonSaveResponse response = new PersonSaveResponse();
        response.errors = Collections.singletonList("unknown color 'blred'");
        response.savedDTOs = Collections.singletonList(createDTO());
        response.status = PersonSavingStatus.FAILED;

        ObjectMapper objectMapper = new ObjectMapper();
        String dtoString = objectMapper.writeValueAsString(response);
        PersonSaveResponse personDTOFromJson = objectMapper.readValue(dtoString, PersonSaveResponse.class);

        assertThat(personDTOFromJson).isEqualToComparingFieldByFieldRecursively(response);
    }

    private PersonDTO createDTO() {
        PersonDTO dto = new PersonDTO();
        dto.name = "Doe";
        dto.surname = "John";
        dto.zipcode = "10004";
        dto.city = "New York";
        dto.color = "Blue";

        return dto;
    }
}