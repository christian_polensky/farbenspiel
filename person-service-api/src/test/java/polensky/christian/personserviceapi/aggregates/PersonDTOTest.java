package polensky.christian.personserviceapi.aggregates;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class PersonDTOTest {

    @Test
    void mustBeSerializableAndDeserializable() throws IOException {
        PersonDTO dto = new PersonDTO();
        dto.name = "Doe";
        dto.surname = "John";
        dto.zipcode = "10004";
        dto.city = "New York";
        dto.color = "Blue";

        ObjectMapper objectMapper = new ObjectMapper();
        String dtoString = objectMapper.writeValueAsString(dto);
        PersonDTO personDTOFromJson = objectMapper.readValue(dtoString, PersonDTO.class);

        assertThat(personDTOFromJson).isEqualToComparingFieldByField(dto);
    }
}