package polensky.christian.personservice.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import polensky.christian.personservice.PersonTestUtils;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class PersonRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private PersonRepository personRepository;

    private Person expectedPerson;
    private Color color;

    @BeforeEach
    public void setup() {
        PersonTestUtils personTestUtils = new PersonTestUtils();
        expectedPerson = personTestUtils.createPerson();
        color = new Color();
        color.setColor("Red");

        entityManager.persist(color);
        entityManager.flush();
        expectedPerson.setColor(color);
        entityManager.persist(expectedPerson);
        entityManager.flush();
    }

    @Test
    void findByColor() {
        List<Person> persons = personRepository.findByColor(color);

        assertThat(persons).hasSize(1);
        assertThat(persons.get(0)).isEqualToComparingFieldByFieldRecursively(expectedPerson);
    }

    @Test
    void findById() {
        Optional<Person> person = personRepository.findById(expectedPerson.getId());
        assertThat(person).isPresent();
        assertThat(person.get()).isEqualToComparingFieldByFieldRecursively(expectedPerson);
    }

    @Test
    void findAll() {
        Person secondPerson = new Person();
        secondPerson.setName("Müller");
        secondPerson.setSurname("Hans");
        secondPerson = entityManager.persist(secondPerson);
        entityManager.flush();

        Iterable<Person> allPersons = personRepository.findAll();

        assertThat(allPersons).hasSize(2);
        assertThat(allPersons).contains(secondPerson);
    }
}