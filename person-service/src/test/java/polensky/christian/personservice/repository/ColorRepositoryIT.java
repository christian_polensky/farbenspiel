package polensky.christian.personservice.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import polensky.christian.personservice.PersonTestUtils;
import polensky.christian.personservice.entity.Color;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class ColorRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ColorRepository colorRepository;

    @Test
    void findByColor() {
        Color color = new PersonTestUtils().crateColor();
        colorRepository.save(color);

        Color savedColor = colorRepository.findByColor(color.getColor());

        assertThat(savedColor).isEqualToComparingFieldByFieldRecursively(color);
    }
}