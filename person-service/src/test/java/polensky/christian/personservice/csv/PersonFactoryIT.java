package polensky.christian.personservice.csv;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import polensky.christian.personservice.PersonTestUtils;
import polensky.christian.personservice.repository.ColorRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PersonFactoryIT {

    @Autowired
    PersonFactory personFactory;
    @MockBean
    ColorRepository colorRepository;

    @BeforeEach
    void setup() {
        Mockito.when(colorRepository.findById(Mockito.any()))
                .thenReturn(Optional.of(new PersonTestUtils().crateColor()));
    }

    @Test
    void fromCsv() {
        PersonTestUtils personTestUtils = new PersonTestUtils();
        CsvPerson csvPerson = personTestUtils.createCsvPerson();

        assertThat(personFactory.fromCsv(csvPerson))
                .isEqualToComparingFieldByFieldRecursively(personTestUtils.createPerson());
    }
}