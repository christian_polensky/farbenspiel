package polensky.christian.personservice.csv;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonFieldsTest {

    @Test
    void getPropertyName() {
        assertThat(PersonFields.COLOR.getPropertyName()).isEqualTo("color");
    }

    @Test
    void getFieldColumn() {
        assertThat(PersonFields.SURNAME.getFieldColumn()).isEqualTo(1);
    }
}