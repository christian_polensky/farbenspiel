package polensky.christian.personservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import polensky.christian.personservice.PersonTestUtils;
import polensky.christian.personservice.controller.PersonDTOFactory;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.repository.ColorRepository;
import polensky.christian.personservice.repository.PersonRepository;
import polensky.christian.personserviceapi.aggregates.PersonDTO;
import polensky.christian.personserviceapi.aggregates.PersonSaveResponse;
import polensky.christian.personserviceapi.aggregates.PersonSavingStatus;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
class PersonServiceIT {

    @Autowired
    PersonService personService;
    @Autowired
    PersonDTOFactory personDTOFactory;
    @MockBean
    PersonRepository personRepository;
    @MockBean
    ColorRepository colorRepository;

    private Person person;
    private List<Person> persons = new ArrayList<>();
    private PersonDTO personDto;
    private Color color;

    @BeforeEach
    void setup() {
        PersonTestUtils personTestUtils = new PersonTestUtils();
        person = personTestUtils.createPerson();

        Person person2 = personTestUtils.createPerson();
        person2.setName("Smith");

        Person person3 = personTestUtils.createPerson();
        person3.setName("Sinclair");

        persons.addAll(Arrays.asList(person, person2, person3));

        personDto = personTestUtils.createPersonDTO();
        color = new Color();
        color.setId(1);
        color.setColor("Red");
    }

    @Test
    void getPerson() {
        when(personRepository.findById(any())).thenReturn(Optional.of(person));
        PersonDTO dto = personService.getPerson(21L).getBody();
        PersonDTO expectedDto = new PersonTestUtils().createPersonDTO();
        assertThat(dto).isEqualToComparingFieldByFieldRecursively(expectedDto);
    }

    @Test
    void getPersons() {
        when(personRepository.findAll()).thenReturn(persons);
        assertThat(personService.getPersons()).isNotEmpty();
        assertThat(personService.getPersons()).usingFieldByFieldElementComparator()
                .isEqualTo(new PersonTestUtils().createPersonDTOs());
    }

    @Test
    void savePersons() {

        PersonSaveResponse response = new PersonSaveResponse();
        response.errors = new ArrayList<>();
        response.savedDTOs = new PersonTestUtils().createPersonDTOs();
        response.status = PersonSavingStatus.SUCCESS;

        when(personRepository.save(any(Person.class))).thenAnswer((Answer<Person>) invocationOnMock -> {
            Object[] args = invocationOnMock.getArguments();
            return (Person) args[0];
        });
        when(colorRepository.findByColor(any())).thenReturn(color);

        assertThat(personService.savePersons(response.savedDTOs))
                .isEqualToComparingFieldByFieldRecursively(response);
    }

    @Test
    void getPersonByColor() {
        when(colorRepository.findByColor(anyString())).thenReturn(color);
        when(personRepository.findByColor(color)).thenReturn(Collections.singletonList(person));

        List<PersonDTO> persons = personService.getPersonByColor("Red");

        assertThat(persons).isNotEmpty();
        assertThat(persons).usingFieldByFieldElementComparator().isEqualTo(Collections.singletonList(personDto));
    }
}