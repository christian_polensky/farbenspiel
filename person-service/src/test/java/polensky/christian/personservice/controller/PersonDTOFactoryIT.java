package polensky.christian.personservice.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import polensky.christian.personservice.PersonTestUtils;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.exception.UnknownColorException;
import polensky.christian.personservice.repository.ColorRepository;
import polensky.christian.personserviceapi.aggregates.PersonDTO;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PersonDTOFactoryIT {

    @Autowired
    private PersonDTOFactory personDTOFactory;
    @MockBean
    private ColorRepository colorRepository;

    private Person expectedPerson;
    private PersonDTO expectedPersonDTO;

    @BeforeEach
    void setup() {
        Mockito.when(colorRepository.findByColor(Mockito.any()))
                .thenReturn(new PersonTestUtils().crateColor());

        expectedPerson = new PersonTestUtils().createPerson();
        expectedPersonDTO = new PersonTestUtils().createPersonDTO();
    }

    @Test
    void fromPerson() {
        PersonDTO personDTO = personDTOFactory.fromPerson(expectedPerson);
        assertThat(personDTO).isEqualToComparingFieldByFieldRecursively(expectedPersonDTO);
    }

    @Test
    void fromPersonList() {
        List<Person> persons = Arrays.asList(expectedPerson, expectedPerson, expectedPerson);
        List<PersonDTO> personDTOS = personDTOFactory.fromPersonList(persons);
        List<PersonDTO> expectedDTOs = Arrays.asList(expectedPersonDTO, expectedPersonDTO, expectedPersonDTO);
        assertThat(personDTOS).hasSize(3);
        assertThat(personDTOS).usingRecursiveFieldByFieldElementComparator().isEqualTo(expectedDTOs);
    }

    @Test
    void toPerson() throws UnknownColorException {
        Person person = personDTOFactory.toPerson(expectedPersonDTO);
        assertThat(person).isEqualToComparingFieldByFieldRecursively(expectedPerson);
    }
}