package polensky.christian.personservice;

import polensky.christian.personservice.csv.CsvPerson;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personserviceapi.aggregates.PersonDTO;

import java.util.ArrayList;
import java.util.List;

public class PersonTestUtils {

    public CsvPerson createCsvPerson() {
        CsvPerson csvPerson = new CsvPerson();
        csvPerson.name = "Doe";
        csvPerson.surname = "John";
        csvPerson.zipCity = "10004 New York";
        csvPerson.color = "1";
        return csvPerson;
    }

    public Person createPerson() {
        final Person person = new Person();
        person.setName("Doe");
        person.setSurname("John");
        person.setZipcode("10004");
        person.setCity("New York");
        Color color = new Color();
        color.setColor("Red");
        color.setId(1);
        person.setColor(color);
        return person;
    }

    public Color crateColor() {
        Color color = new Color();
        color.setColor("Red");
        color.setId(1);
        return color;
    }

    public PersonDTO createPersonDTO() {
        PersonDTO dto = new PersonDTO();
        dto.name = "Doe";
        dto.surname = "John";
        dto.zipcode = "10004";
        dto.city = "New York";
        dto.color = "Red";
        return dto;
    }

    public List<PersonDTO> createPersonDTOs() {
        List<PersonDTO> dtos = new ArrayList<>();
        PersonDTO dto = new PersonDTO();
        dto.name = "Doe";
        dto.surname = "John";
        dto.zipcode = "10004";
        dto.city = "New York";
        dto.color = "Red";

        dtos.add(copyDTO(dto));
        dto.name = "Smith";
        dtos.add(copyDTO(dto));
        dto.name = "Sinclair";
        dtos.add(copyDTO(dto));

        return dtos;
    }

    private PersonDTO copyDTO(PersonDTO origTDO) {
        PersonDTO dto = new PersonDTO();
        dto.name = origTDO.name;
        dto.surname = origTDO.surname;
        dto.zipcode = origTDO.zipcode;
        dto.city = origTDO.city;
        dto.color = origTDO.color;
        return dto;
    }
}
