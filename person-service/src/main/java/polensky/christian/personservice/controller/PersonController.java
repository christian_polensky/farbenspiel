package polensky.christian.personservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import polensky.christian.personservice.service.PersonCsvService;
import polensky.christian.personservice.service.PersonService;
import polensky.christian.personserviceapi.aggregates.PersonDTO;
import polensky.christian.personserviceapi.aggregates.PersonSaveResponse;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Component
@RestController
@RequestMapping("persons")
public class PersonController {

    private final PersonService service;
    private final PersonCsvService csvService;

    @Autowired
    public PersonController(PersonService service, PersonCsvService csvService) {

        this.service = service;
        this.csvService = csvService;
    }

    @GetMapping("/{id}")
    public @ResponseBody ResponseEntity<PersonDTO> getPerson(@PathVariable long id) {
        return service.getPerson(id);
    }

    @GetMapping("")
    public @ResponseBody List<PersonDTO> getPersons() {
        return service.getPersons();
    }

    @PostMapping("")
    public @ResponseBody PersonSaveResponse savePersons(List<PersonDTO> personDTOS) {
        return service.savePersons(personDTOS);
    }

    @GetMapping("/color/{colorString}")
    public @ResponseBody List<PersonDTO> getPersonByColor(@PathVariable String colorString) {
        return service.getPersonByColor(colorString);
    }

    @GetMapping("/importCSV")
    public @ResponseBody boolean importCSV()
            throws IllegalAccessException, IOException, InstantiationException, NoSuchMethodException, InvocationTargetException
    {
        return csvService.importPersonCsv();
    }
}
