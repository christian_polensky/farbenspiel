package polensky.christian.personservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.repository.ColorRepository;
import polensky.christian.personserviceapi.aggregates.PersonDTO;
import polensky.christian.personservice.exception.UnknownColorException;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonDTOFactory {

    final private ColorRepository colorRepository;

    @Autowired
    public PersonDTOFactory(ColorRepository colorRepository) {
        this.colorRepository = colorRepository;
    }

    public PersonDTO fromPerson(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.name = person.getName();
        personDTO.surname = person.getSurname();
        personDTO.zipcode = person.getZipcode();
        personDTO.city = person.getCity();
        personDTO.color = person.getColor().getColor();
        return personDTO;
    }

    public List<PersonDTO> fromPersonList(Iterable<Person> persons) {
        List<PersonDTO> personDTOs = new ArrayList<>();
        for (Person person : persons) {
            personDTOs.add(fromPerson(person));
        }
        return personDTOs;
    }

    public Person toPerson(PersonDTO personDTO) throws UnknownColorException {
        Person person = new Person();
        person.setName(personDTO.name);
        person.setSurname(personDTO.surname);
        person.setZipcode(personDTO.zipcode);
        person.setCity(personDTO.city);
        person.setColor(getColorFromColorString(personDTO.color));
        return person;
    }

    private Color getColorFromColorString(String colorString) throws UnknownColorException {
        Color color = colorRepository.findByColor(colorString);
        if (color == null) {
            throw new UnknownColorException(String.format("unknown color '%s'", colorString));
        }
        return color;
    }
}
