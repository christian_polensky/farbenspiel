package polensky.christian.personservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import polensky.christian.personservice.entity.Color;

@Repository
public interface ColorRepository extends CrudRepository<Color, Integer> {
    Color findByColor(String color);
}
