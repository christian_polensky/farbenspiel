package polensky.christian.personservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    List<Person> findByColor(Color color);
}
