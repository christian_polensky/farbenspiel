package polensky.christian.personservice.exception;

public class UnknownColorException extends Exception {
    public UnknownColorException(String message) {
        super(message);
    }
}
