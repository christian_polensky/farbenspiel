package polensky.christian.personservice.csv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.repository.ColorRepository;

@Component
public class PersonFactory {

    private final ColorRepository colorRepository;

    @Autowired
    public PersonFactory(ColorRepository colorRepository) {
        this.colorRepository = colorRepository;
    }

    public Person fromCsv(CsvPerson csvPerson) {
        final Person person = new Person();
        person.setName(csvPerson.name);
        person.setSurname(csvPerson.surname);
        person.setZipcode(getZipFromCsvPerson(csvPerson));
        person.setCity(getCityFromCsvPerson(csvPerson));
        person.setColor(colorRepository.findById(Integer.valueOf(csvPerson.color)).orElse(null));
        return person;
    }

    private String getCityFromCsvPerson(CsvPerson csvPerson) {
        return getSplittedZipCity(csvPerson)[1];
    }

    private String getZipFromCsvPerson(CsvPerson csvPerson) {
        return getSplittedZipCity(csvPerson)[0];
    }

    private String[] getSplittedZipCity(CsvPerson csvPerson) {
        return csvPerson.zipCity.split(" ", 2);
    }
}
