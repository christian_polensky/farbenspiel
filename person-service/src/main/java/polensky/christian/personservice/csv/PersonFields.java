package polensky.christian.personservice.csv;

import polensky.christian.csvparser.CSVParserFieldEnum;

public enum PersonFields implements CSVParserFieldEnum {

    NAME("name", 0), SURNAME("surname", 1), ZIP_CITY("zipCity", 2), COLOR("color", 3);

    private String propertyName;
    private int fieldColumn;

    PersonFields(String propertyName, int fieldColumn) {
        this.propertyName = propertyName;
        this.fieldColumn = fieldColumn;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public int getFieldColumn() {
        return fieldColumn;
    }
}
