package polensky.christian.personservice.csv;

import polensky.christian.csvparser.CSVParsedEntity;
import polensky.christian.csvparser.CSVParserFieldEnum;

import java.util.EnumSet;
import java.util.stream.Stream;

public class CsvPerson implements CSVParsedEntity {

    public String name;
    public String surname;
    public String zipCity;
    public String color;

    @Override
    public Stream<? extends CSVParserFieldEnum> getCsvParsingFields() {
        return EnumSet.allOf(PersonFields.class).stream();
    }
}
