package polensky.christian.personservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import polensky.christian.csvparser.CsvParser;
import polensky.christian.personservice.csv.CsvPerson;
import polensky.christian.personservice.csv.PersonFactory;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.repository.ColorRepository;
import polensky.christian.personservice.repository.PersonRepository;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public class PersonCsvService {

    private final PersonRepository personRepository;
    private final PersonFactory personFactory;
    private ColorRepository colorRepository;

    @Autowired
    public PersonCsvService(PersonRepository personRepository, PersonFactory personFactory, ColorRepository colorRepository) {
        this.personRepository = personRepository;
        this.personFactory = personFactory;
        this.colorRepository = colorRepository;
    }

    public boolean importPersonCsv()
            throws IllegalAccessException, IOException, InstantiationException, NoSuchMethodException, InvocationTargetException
    {
        final List<Person> persons = getPersonsFromCsv();
        if (persons.isEmpty()) return false;
        return importPersons(persons);
    }

    private List<Person> getPersonsFromCsv()
            throws IllegalAccessException, InstantiationException, IOException, NoSuchMethodException, InvocationTargetException
    {
        final ClassLoader classLoader = PersonCsvService.class.getClassLoader();
        final String csvPath = classLoader.getResource("file/persons.csv").getPath();
        final List<CsvPerson> csvPersons = new CsvParser<CsvPerson>().parse(CsvPerson.class, csvPath);

        final List<Person> persons = new ArrayList<>();
        for (CsvPerson csvPerson : csvPersons) {
            persons.add(personFactory.fromCsv(csvPerson));
        }
        return persons;
    }

    private boolean importPersons(List<Person> persons) {
        Iterable<Person> savedPersons = personRepository.saveAll(persons);
        return Stream.of(savedPersons).count() > 0;
    }

}
