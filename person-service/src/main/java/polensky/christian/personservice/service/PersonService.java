package polensky.christian.personservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import polensky.christian.personservice.controller.PersonDTOFactory;
import polensky.christian.personservice.entity.Color;
import polensky.christian.personservice.entity.Person;
import polensky.christian.personservice.exception.UnknownColorException;
import polensky.christian.personservice.repository.ColorRepository;
import polensky.christian.personservice.repository.PersonRepository;
import polensky.christian.personserviceapi.aggregates.PersonDTO;
import polensky.christian.personserviceapi.aggregates.PersonSaveResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static polensky.christian.personserviceapi.aggregates.PersonSavingStatus.*;

@Component
public class PersonService {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    ColorRepository colorRepository;
    @Autowired
    PersonCsvService personCsvService;
    @Autowired
    PersonDTOFactory personDTOFactory;

    public ResponseEntity<PersonDTO> getPerson(@PathVariable long id) {
        return handleOptional(personRepository.findById(id));
    }

    public List<PersonDTO> getPersons() {
        return personDTOFactory.fromPersonList(personRepository.findAll());
    }

    public PersonSaveResponse savePersons(List<PersonDTO> personDTOS) {
        PersonSaveResponse response = new PersonSaveResponse();
        List<PersonDTO> savedDTOs = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        for (PersonDTO personDTO : personDTOS) {
            try {
                Person person = personDTOFactory.toPerson(personDTO);
                savedDTOs.add(personDTOFactory.fromPerson(personRepository.save(person)));
            } catch (UnknownColorException e) {
                errors.add(e.getMessage());
            }
        }

        if (errors.size() > 0) {
            if (savedDTOs.size() > 0) {
                response.status = PARTIALLY_FAILED;
            } else {
                response.status = FAILED;
            }
        } else {
            response.status = SUCCESS;
        }

        response.errors = errors;
        response.savedDTOs = savedDTOs;
        return response;
    }

    public List<PersonDTO> getPersonByColor(@PathVariable String colorString) {
        final Color color = colorRepository.findByColor(colorString);
        return personDTOFactory.fromPersonList(personRepository.findByColor(color));
    }

    private ResponseEntity<PersonDTO> handleOptional(Optional<Person> person) {
        if (person.isPresent()) {
            return ResponseEntity.ok(personDTOFactory.fromPerson(person.get()));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
