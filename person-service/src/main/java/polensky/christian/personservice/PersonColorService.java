package polensky.christian.personservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonColorService {
    public static void main(String[] args) {
        SpringApplication.run(PersonColorService.class, args);
    }
}
