package polensky.christian.csvparser;

import java.util.stream.Stream;

public interface CSVParsedEntity {

    Stream<? extends CSVParserFieldEnum> getCsvParsingFields();
}
