package polensky.christian.csvparser;

public interface CSVParserFieldEnum {

    String getPropertyName();
    int getFieldColumn();
}
