package polensky.christian.csvparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CsvParser<T extends CSVParsedEntity> {

    private static Logger logger = LoggerFactory.getLogger(CsvParser.class);

    public List<T> parse(Class<T> objectClass, String csvPath)
            throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        List<T> parsedObjects = new ArrayList<>();
        Reader reader = Files.newBufferedReader(Paths.get(csvPath));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

        for (CSVRecord record : csvParser) {
            T object = objectClass.getDeclaredConstructor().newInstance();
            recordToObject(record, object);
            parsedObjects.add(object);
        }

        return parsedObjects;
    }

    private void recordToObject(CSVRecord record, T object) {
        object.getCsvParsingFields().forEach(field -> setRecordFieldToProperty(field, object, record));
    }

    private T setRecordFieldToProperty(CSVParserFieldEnum fieldEnum, T object, CSVRecord record) {
        String propertyName = fieldEnum.getPropertyName();
        int fieldColumn = fieldEnum.getFieldColumn();
        String value = record.get(fieldColumn).trim();

        try {
            Field field = object.getClass().getDeclaredField(propertyName);
            field.setAccessible(true);
            field.set(object, value);
        } catch (IllegalAccessException e) {
            String message = String.format("problem with access of '%s' in class %s", propertyName, object.getClass().getSimpleName());
            logger.error(message, e);
            throw new RuntimeException();
        } catch (NoSuchFieldException e) {
            String message = String.format("couldn't find field '%s' in class %s", propertyName, object.getClass().getSimpleName());
            logger.error(message, e);
            throw new RuntimeException();
        }
        return object;
    }
}
