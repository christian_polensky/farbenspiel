package polensky.christian.csvparser;

public enum PersonFields implements CSVParserFieldEnum {

    NAME("name", 0), AGE("age", 1), HEIGHT("height", 2);

    private String propertyName;
    private int fieldColumn;

    PersonFields(String propertyName, int fieldColumn) {
        this.propertyName = propertyName;
        this.fieldColumn = fieldColumn;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public int getFieldColumn() {
        return fieldColumn;
    }
}
