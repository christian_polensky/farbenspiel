package polensky.christian.csvparser;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CsvParserTest {

    private static String csvPath;

    @BeforeAll
    static void setupAll() {
        ClassLoader classLoader = CsvParserTest.class.getClassLoader();
        csvPath = classLoader.getResource("csvParserTestFile.csv").getPath();
    }

    @Test
    void csvMustNotBeParsed()
            throws InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, InvocationTargetException
    {
        CsvParser<Person> parser = new CsvParser<>();
        Person chris = new PersonBuilder()
                .withName("Chris")
                .withAge("34")
                .withHeight("176")
                .build();
        Person alex = new PersonBuilder()
                .withName("Alexander")
                .withAge("15")
                .withHeight("180")
                .build();
        Person sandra = new PersonBuilder()
                .withName("Sandra")
                .withAge("24")
                .withHeight("165")
                .build();

        List<Person> persons = parser.parse(Person.class, csvPath);

        assertThat(persons).isNotNull();
        assertThat(persons).isNotEmpty();
        assertThat(persons.get(0)).isEqualToComparingFieldByField(chris);
        assertThat(persons.get(1)).isEqualToComparingFieldByField(alex);
        assertThat(persons.get(2)).isEqualToComparingFieldByField(sandra);
    }
}
