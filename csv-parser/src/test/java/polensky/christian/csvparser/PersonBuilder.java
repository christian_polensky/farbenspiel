package polensky.christian.csvparser;

class PersonBuilder {

    private String name;
    private String age;
    private String height;

    PersonBuilder() {
    }

    PersonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    PersonBuilder withAge(String age) {
        this.age = age;
        return this;
    }

    PersonBuilder withHeight(String height) {
        this.height = height;
        return this;
    }

    Person build() {
        Person person = new Person();
        person.setName(name);
        person.setAge(age);
        person.setHeight(height);
        return person;
    }
}
