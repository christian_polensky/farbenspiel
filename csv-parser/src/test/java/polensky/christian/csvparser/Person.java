package polensky.christian.csvparser;

import java.util.EnumSet;
import java.util.stream.Stream;

public class Person implements CSVParsedEntity{

    private String name;
    private String age;
    private String height;

    public Stream<? extends CSVParserFieldEnum> getCsvParsingFields() {
        return EnumSet.allOf(PersonFields.class).stream();
    }

    void setName(String name) {
        this.name = name;
    }

    void setAge(String age) {
        this.age = age;
    }

    void setHeight(String height) {
        this.height = height;
    }

}

